package nl.naturalis.oaipmh.api;

/**
 * RuntimeException subclass that can be thrown by implementations when processing OAI-PMH
 * requests.
 *
 * @author Ayco Holleman
 */
public class OaiRuntimeException extends RuntimeException {

  public OaiRuntimeException(String message) {
    super(message);
  }

  public OaiRuntimeException(Throwable cause) {
    super(cause);
  }

  public OaiRuntimeException(String message, Throwable cause) {
    super(message, cause);
  }

}

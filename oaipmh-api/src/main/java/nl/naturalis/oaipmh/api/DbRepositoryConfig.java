package nl.naturalis.oaipmh.api;

/**
 * A Configuration object for OAI repositories that use a database for data storage. This class is also serves as a factory for JDBC
 * connections and connection pools (using the Hikari library).
 *
 * @author Ayco Holleman
 */
public abstract class DbRepositoryConfig extends RepositoryConfig {

  private DatabaseConfig database;

  public DatabaseConfig getDatabase() {
    return database;
  }

  public void setDatabase(DatabaseConfig database) {
    this.database = database;
  }

}

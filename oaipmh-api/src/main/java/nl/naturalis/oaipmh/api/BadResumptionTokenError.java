package nl.naturalis.oaipmh.api;

import static org.openarchives.oai._2.OAIPMHerrorcodeType.BAD_RESUMPTION_TOKEN;

import java.util.function.Supplier;

import org.openarchives.oai._2.OAIPMHerrorType;

/**
 * Convenience class narrowing the JAXB {@code OAIPMHerrorType} class to one for
 * BadResumptionToken errors.
 *
 * @author Ayco Holleman
 *
 */
public class BadResumptionTokenError extends OAIPMHerrorType {

	public static Supplier<OaiProtocolException> recordOffsetOutOfRange() {
		return () -> because("Record offset out of range");
	}

	public static Supplier<OaiProtocolException> invalidNumberOfElementsInToken() {
		return () -> because("Invalid number of elements in resumption token");
	}

	public static OaiProtocolException because(String reason) {
		return new OaiProtocolException(new BadResumptionTokenError(reason));
	}

	public BadResumptionTokenError() {
		this("Error parsing resumption token");
	}

	public BadResumptionTokenError(String message) {
		super();
		this.code = BAD_RESUMPTION_TOKEN;
		this.value = message;
	}

}

package nl.naturalis.oaipmh.api.util;

import com.google.common.annotations.VisibleForTesting;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import nl.naturalis.oaipmh.api.DatabaseConfig;
import nl.naturalis.oaipmh.api.DbRepositoryConfig;
import org.klojang.check.Check;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import static org.klojang.check.CommonChecks.notNull;
import static org.klojang.util.ObjectMethods.ifNull;
import static org.klojang.util.StringMethods.ifBlank;
import static org.klojang.util.StringMethods.isBlank;

/**
 * A factory for JDBC connections and JDBC connection pools. Participating OAI
 * repositories can make use of this by configuring themselves using a
 * {@link DbRepositoryConfig} instance. In other words, their entry in the server
 * configuration file (oaipmh-server.yaml) has the shape of the {@link DbRepositoryConfig}
 * class.
 *
 * @author Ayco Holleman
 */
public class Database {

  private static final HashMap<DatabaseConfig, Database> cache = new HashMap<>();
  private static final String ERR_MISSING_TYPE = "Missing \"type\" property in database configuration";

  /**
   * Returns the <code>Database</code> with the provided configuration. Note that the
   * <code>hashCode</code> and <code>equals</code> implementations of
   * {@link DatabaseConfig} uniquely identify a single database connection.
   *
   * @param config
   * @return
   */
  public static final Database withConfig(DatabaseConfig config) {
    return cache.computeIfAbsent(config, Database::new);
  }

  private final DatabaseConfig config;

  private HikariDataSource pool;

  private Database(DatabaseConfig config) {
    this.config = config;
  }

  /**
   * Creates and returns a new JDBC connection. The connection is not managed; you must
   * close it yourself.
   *
   * @return
   * @throws SQLException
   */
  public Connection connect() throws SQLException {
    Check.that(config.getType()).is(notNull(), ERR_MISSING_TYPE);
    Properties props = new Properties();
    if (!isBlank(config.getUser())) {
      props.put("user", config.getUser());
    }
    if (!isBlank(config.getPassword())) {
      props.put("password", config.getPassword());
    }
    return DriverManager.getConnection(getJdbcUrl(), props);
  }

  /**
   * Returns a Hikari JDBC connection pool. The first call to this method instantiates the
   * connection pool. Subsequent calls return that same connection pool.
   *
   * @return
   */
  public HikariDataSource getConnectionPool() {
    Check.that(config.getType()).is(notNull(), ERR_MISSING_TYPE);
    if (pool == null) {
      HikariConfig hikari = new HikariConfig();
      hikari.setJdbcUrl(getJdbcUrl());
      hikari.setUsername(config.getUser());
      hikari.setPassword(config.getPassword());
      hikari.setMinimumIdle(config.getMinPoolSize());
      hikari.setMaximumPoolSize(config.getMaxPoolSize());
      hikari.setConnectionTimeout(10 * 60 * 1000);
      hikari.setReadOnly(true);
      hikari.setAutoCommit(false);
      pool = new HikariDataSource(hikari);
    }
    return pool;
  }

  /**
   * Closes the connection pool created and cached by this instance. If no connection pool
   * was ever requested from this instance, this method does nothing.
   */
  public void shutDown() {
    if (pool != null) {
      try {
        pool.close();
      } finally {
        pool = null;
        cache.clear();
      }
    }
  }

  @VisibleForTesting
  String getJdbcUrl() {
    String host = ifBlank(config.getHost(), "localhost");
    int port = ifNull(config.getPort(), config.getType().getDefaultPort());
    return config.getType().getJdbcUrlTemplate()
          .replace("{host}", host)
          .replace("{port}", String.valueOf(port))
          .replace("{db}", config.getName());
  }

}

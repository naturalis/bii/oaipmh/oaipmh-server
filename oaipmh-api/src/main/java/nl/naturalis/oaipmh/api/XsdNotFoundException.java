package nl.naturalis.oaipmh.api;

/**
 * Thrown by
 * {@link StandAloneRepository#getXSDForMetadataPrefix(java.io.OutputStream, String)} if
 * the requested XSD cannot be served.
 *
 * @author Ayco Holleman
 */
public class XsdNotFoundException extends RepositoryException {

  public XsdNotFoundException(String nsPrefix) {
    super("No XSD found for XML namespace prefix \"" + nsPrefix + "\"");
  }

}

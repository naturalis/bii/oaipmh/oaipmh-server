package nl.naturalis.geneious.oaipmh;

import org.w3c.dom.Element;

/**
 * Factory for {@link Ab1Document} instances.
 *
 * @author Ayco Holleman
 */
final class Ab1DocumentFactory {

  Ab1DocumentFactory() { }

  /**
   * Creates a new {@link Ab1Document} instance from the specified XML root element, which
   * has been created from the plugin_document_xml column in the annotated_document
   * table.
   *
   * @param root
   * @return
   */
  Ab1Document build(Element root) {
    return new Ab1Document();
  }

}

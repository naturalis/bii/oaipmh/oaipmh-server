package nl.naturalis.oaipmh.api.util;

import org.junit.Test;
import nl.naturalis.oaipmh.api.DatabaseConfig;
import nl.naturalis.oaipmh.api.DatabaseType;
import static org.junit.Assert.assertEquals;

public class DatabaseTest {

  @Test
  public void getPostgresJdbcUrl() {
    DatabaseConfig config = new DatabaseConfig();
    config.setType(DatabaseType.POSTGRES);
    config.setHost("123.456.789.123");
    config.setName("the_database");
    Database db = Database.withConfig(config);
    assertEquals("jdbc:postgresql://123.456.789.123:5432/the_database", db.getJdbcUrl());

    config = new DatabaseConfig();
    config.setType(DatabaseType.POSTGRES);
    config.setHost(null);
    config.setPort(7777);
    config.setName("the_database");
    db = Database.withConfig(config);
    assertEquals("jdbc:postgresql://localhost:7777/the_database", db.getJdbcUrl());
  }

  @Test
  public void getMariaDBJdbcUrl() {
    DatabaseConfig config = new DatabaseConfig();
    config.setType(DatabaseType.MARIADB);
    config.setHost("123.456.789.123");
    config.setName("the_database");
    Database db = Database.withConfig(config);
    assertEquals("jdbc:mariadb://123.456.789.123:3306/the_database", db.getJdbcUrl());
    
    config = new DatabaseConfig();
    config.setType(DatabaseType.MARIADB);
    config.setHost(null);
    config.setPort(7777);
    config.setName("the_database");
    db = Database.withConfig(config);
    assertEquals("jdbc:mariadb://localhost:7777/the_database", db.getJdbcUrl());
  }

}

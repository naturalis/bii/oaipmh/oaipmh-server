package nl.naturalis.oaipmh.api;

import org.openarchives.oai._2.VerbType;

import java.util.EnumMap;
import java.util.Set;

import static org.openarchives.oai._2.VerbType.*;

/**
 * Creates {@link ArgumentValidator} instances.
 *
 * @author Ayco Holleman
 */
public class ArgumentValidatorFactory {

  public static final ArgumentValidatorFactory INSTANCE = new ArgumentValidatorFactory();

  private final EnumMap<VerbType, ArgumentValidator> validators;

  private ArgumentValidatorFactory() {
    validators = new EnumMap<>(VerbType.class);
    validators.put(LIST_RECORDS, new ListRecordsArgumentValidator());
    validators.put(GET_RECORD, new GetRecordArgumentValidator());
    validators.put(LIST_SETS, new ListSetsArgumentValidator());
  }

  /**
   * Creates an {@link ArgumentValidator} instance for the specified verb.
   *
   * @param verb
   * @return
   */
  public ArgumentValidator getValidatorForVerb(VerbType verb) {
    if (validators.containsKey(verb)) {
      return validators.get(verb);
    }
    throw new UnsupportedOperationException("Verb not supported: " + verb.value());
  }

  /**
   * Returns all OAI-PMH verbs currently supported by the OAI-PMH service.
   *
   * @return
   */
  public Set<VerbType> getSupportedVerbs() {
    return validators.keySet();
  }

}

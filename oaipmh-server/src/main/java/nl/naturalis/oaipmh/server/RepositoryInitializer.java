package nl.naturalis.oaipmh.server;

import org.klojang.check.Check;
import nl.naturalis.oaipmh.api.ManagedRepository;
import nl.naturalis.oaipmh.api.RepositoryGroup;
import nl.naturalis.oaipmh.api.RepositoryInitializationException;
import nl.naturalis.oaipmh.api.StandAloneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;
import static nl.naturalis.check.CommonChecks.blank;
import static nl.naturalis.check.CommonChecks.notNull;
import static nl.naturalis.check.CommonExceptions.STATE;
import static nl.naturalis.oaipmh.api.RepositoryInitializationException.implementationError;
import static nl.naturalis.oaipmh.api.RepositoryInitializationException.noSuchRepository;

abstract sealed class RepositoryInitializer
      permits DefaultRepositoryInitializer, ClassGraphRepositoryInitializer {

  private static final Logger logger = LoggerFactory.getLogger(RepositoryInitializer.class);

  private final OaiConfig cfg;

  private List<ManagedRepository> repos;

  RepositoryInitializer(OaiConfig cfg) {
    this.cfg = cfg;
  }

  void initialize() throws RepositoryInitializationException {

    logger.info("Searching for OAI repositories");
    List<ManagedRepository> instances = findImplementations();
    if (instances.isEmpty()) {
      logger.warn("No repositories found!");
      this.repos = Collections.emptyList();
      return;
    }

    logger.info("Validating repositories");
    for (ManagedRepository r : instances) {
      String name = r.getName();
      logger.info("===> {}", name);
      if (!(r instanceof StandAloneRepository || r instanceof RepositoryGroup)) {
        throw implementationError(r);
      }
      Check.that(name).isNot(
            blank(),
            () -> implementationError(
                  r,
                  "Repository %s does not identify itself by name",
                  r.getClass()));
      if (!cfg.repositories.containsKey(r.getName())) {
        logger.warn("No entry for repository \"{}\" in server configuration file", name);
      }
    }

    Map<String, ManagedRepository> available =
          instances.stream()
                .collect(toMap(ManagedRepository::getName, Function.identity()));

    logger.info("Configuring repositories");
    repos = new ArrayList<>(available.size());
    for (String name : cfg.repositories.keySet()) {
      logger.info("===> {}", name);
      ManagedRepository repo = available.get(name);
      if (repo == null) {
        throw noSuchRepository(name);
      }
      repo.configure(cfg.repositories.get(name));
      if (repo instanceof RepositoryGroup rg) {
        rg.getRepositoryNames().stream().forEach(s -> logger.info("--------> {}", s));
      }
      repos.add(repo);
    }
  }

  abstract List<ManagedRepository> findImplementations()
        throws RepositoryInitializationException;

  List<ManagedRepository> getManagedRepositories() {
    Check.on(STATE, repos).is(notNull(), "Repositories not initialized yet");
    return repos;
  }

  Map<String, StandAloneRepository> getStandAloneRepositories() {
    Check.on(STATE, repos).is(notNull(), "Repositories not initialized yet");
    return repos.stream()
          .filter(r -> r instanceof StandAloneRepository)
          .map(r -> (StandAloneRepository) r)
          .collect(toMap(StandAloneRepository::getName, Function.identity()));
  }

  Map<String, RepositoryGroup> getRepositoryGroups() {
    Check.on(STATE, repos).is(notNull(), "Repositories not initialized yet");
    return repos.stream()
          .filter(r -> r instanceof RepositoryGroup)
          .map(r -> (RepositoryGroup) r)
          .collect(toMap(RepositoryGroup::getName, Function.identity()));
  }
}

package nl.naturalis.oaipmh.api;

import nl.naturalis.common.Bool;
import nl.naturalis.common.NumberMethods;
import org.openarchives.oai._2.VerbType;

import java.net.URI;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.klojang.util.ObjectMethods.isEmpty;
import static org.klojang.util.StringMethods.ensureSuffix;
import static org.klojang.util.StringMethods.substringBefore;

/**
 * Java bean modeling an OAI-PMH request. Instances of this class are the only objects
 * crossing the border from the REST layer to the
 * {@link StandAloneRepository OAI repository}. The REST layer creates them, the OAI
 * repository reads them. Besides the 6 OAI-PMH "arguments" (verb, metadataPrefix,
 * identifier, from, until, set, resumptionToken), this class provides the following extra
 * information:
 * <ol>
 * <li>{@code page} - The requested page within the result set. Used when paging through large data
 * sets using resumption tokens. Note that the &lt;resumptionToken&gt; element within OAI-PMH XML
 * has a {@code cursor} attribute, which is the <i>absolute</i> offset of the first record of the
 * requested page. However, the REST layer does not know what the page size (number of records) for
 * any particular OAI repository is. Therefore, the repository implementation has to do the math
 * itself when setting the {@code cursor} attribute for the &lt;resumptionToken&gt; element
 * ({@code cursor = page * pageSize}).
 * <li>{@code requestUri} - The original HTTP request URL.
 * <li>{@code dateFormatFrom} - The (OAI-PMH compliant) date format used by the client for the from
 * argument. The OAI-PMH protocol supports to types of formatting:
 * {@link DateTimeFormatter#ISO_INSTANT} or {@link DateTimeFormatter#ISO_LOCAL_DATE}. An exception
 * will be thrown if the client uses any other format.
 * <li>{@code dateFormatUntil} - The (OAI-PMH compliant) date format used by the client for the
 * until argument.
 * </ol>
 *
 * @author Ayco Holleman
 */
public class OaiRequest {

  private VerbType verb;
  private String metadataPrefix;
  private String identifier;
  private Instant from;
  private Instant until;
  private List<String> set;
  private String resumptionToken;
  private int page;
  private URI baseUri;
  private URI requestUri;
  private DateTimeFormatter dateFormatFrom;
  private DateTimeFormatter dateFormatUntil;
  private boolean explain;

  private Map<String, List<String>> customParams;

  /**
   * Returns the array index of the first record of the requested page, given the provided
   * page size. Can be used to construct SQL LIMIT clauses.
   *
   * @return
   */
  public int getRecordOffset(int pageSize) {
    return page * pageSize;
  }

  /**
   * Returns the array index of the last record of the requested page, given the provided
   * page size and total number of records (the latter functioning as an upper bound).
   *
   * @return
   */
  public int getStartIndexOfNextPage(int pageSize, int completeListSize) {
    return Math.min(getRecordOffset(pageSize) + pageSize, completeListSize);
  }

  /**
   * Whether or not the current request completes the harvest.
   *
   * @param completeListSize
   * @return
   */
  public boolean isHarvestComplete(int pageSize, int completeListSize) {
    return getRecordOffset(pageSize) + pageSize >= completeListSize;
  }

  public VerbType getVerb() {
    return verb;
  }

  public void setVerb(VerbType verb) {
    this.verb = verb;
  }

  public String getMetadataPrefix() {
    return metadataPrefix;
  }

  public void setMetadataPrefix(String metadataPrefix) {
    this.metadataPrefix = metadataPrefix;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public Instant getFrom() {
    return from;
  }

  public void setFrom(Instant from) {
    this.from = from;
  }

  public Instant getUntil() {
    return until;
  }

  public void setUntil(Instant until) {
    this.until = until;
  }

  public List<String> getSet() {
    return set;
  }

  public void setSet(List<String> set) {
    this.set = set;
  }

  public String getResumptionToken() {
    return resumptionToken;
  }

  public void setResumptionToken(String resumptionToken) {
    this.resumptionToken = resumptionToken;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public URI getBaseUri() {
    return baseUri;
  }

  public void setBaseUri(URI baseUri) {
    this.baseUri = baseUri;
  }

  public URI getRequestUri() {
    return requestUri;
  }

  /**
   * Returns the original request URL with or without query parameters. If without
   * parameters the URL is guaranteed to end with a forward slash ('/').
   *
   * @param stripQueryParams
   * @return
   */
  public URI getRequestUri(boolean stripQueryParams) {
    if (stripQueryParams) {
      return URI.create(ensureSuffix(substringBefore(requestUri, "/", 1), "/"));
    }
    return requestUri;
  }

  public void setRequestUri(URI requestUri) {
    this.requestUri = requestUri;
  }

  public DateTimeFormatter getDateFormatFrom() {
    return dateFormatFrom;
  }

  public void setDateFormatFrom(DateTimeFormatter dateFormatFrom) {
    this.dateFormatFrom = dateFormatFrom;
  }

  public DateTimeFormatter getDateFormatUntil() {
    return dateFormatUntil;
  }

  public void setDateFormatUntil(DateTimeFormatter dateFormatUntil) {
    this.dateFormatUntil = dateFormatUntil;
  }

  /**
   * Whether or not the request is executing within an "explain" context. This will be the
   * case if the request URL contained the <code>__explain</code> query parameter. In this
   * case the regular OAIPMH output is suspended and the output from the loggers is shown
   * instead.
   *
   * @return
   */
  public boolean isExplain() {
    return explain;
  }

  public void setExplain(boolean explain) {
    this.explain = explain;
  }

  /**
   * Returns a map of (URL) query parameters and their values, that are supposedly
   * meaningful to the repository but that do not conform to the OAI-PMH protocol. They
   * are passed on to the OAI repository without being validated (as OAI-PMH arguments).
   * Only query parameters starting with two underscores will be treated as custom
   * parameters. All Other parameters will be validated as OAI-PMH arguments (and may
   * cause the request to fail). The "__explain" and __detail parameters are a special
   * kind of custom parameter. They are also processed by the OAIPMH server itself.
   *
   * @return
   */
  public Map<String, List<String>> getCustomParameters() {
    return customParams;
  }

  public void setCustomParameters(Map<String, List<String>> params) {
    this.customParams = params;
  }

  public void addCustomParameter(String key, List<String> value) {
    if (customParams == null) {
      customParams = new HashMap<>();
    }
    customParams.put(key, value);
  }

  public List<String> getCustomParameter(String key) {
    if (customParams == null) {
      return null;
    }
    return customParams.get(key);
  }

  public int getPageSize(RepositoryConfig config) {
    int i = getPageSize();
    if (i == 0) {
      return config.getPageSize();
    }
    return i;
  }

  public int getPageSize() {
    List<String> l = getCustomParameter("__pageSize");
    if (isEmpty(l) || isEmpty(l.get(0))) {
      return 0;
    }
    return NumberMethods.parseInt(l.get(0));
  }

  public boolean isStayAlive() {
    List<String> l = getCustomParameter("__stayAlive");
    if (isEmpty(l)) {
      return false;
    }
    return isEmpty(l.get(0)) || Bool.from(l.get(0));
  }

  public String getQueryId() {
    if (isStayAlive()) {
      List<String> l = getCustomParameter("__queryId");
      if (!isEmpty(l)) {
        return l.get(0);
      }
    }
    return null;
  }

}

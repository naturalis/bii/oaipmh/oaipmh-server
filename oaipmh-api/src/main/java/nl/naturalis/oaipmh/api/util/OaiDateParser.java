package nl.naturalis.oaipmh.api.util;


import static java.time.ZoneOffset.UTC;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

import nl.naturalis.fuzzydate.FuzzyDate;
import nl.naturalis.fuzzydate.FuzzyDateException;
import nl.naturalis.fuzzydate.FuzzyDateParser;
import nl.naturalis.fuzzydate.ParseAttempt;
import nl.naturalis.oaipmh.api.BadArgumentError;
import nl.naturalis.oaipmh.api.OaiProtocolException;
import nl.naturalis.oaipmh.api.OaiRequest;

/**
 * Utility class for converting between native dates and the two date types supported
 * by the OAI-PMH protocol (ISO_INSTANT and ISO_LOCAL_DATE).
 *
 * @author Ayco Holleman
 */
public class OaiDateParser {

  /**
   * Returns the one and only instance of this class.
   */
  public static final OaiDateParser INSTANCE = new OaiDateParser();

  private static final FuzzyDateParser parser = new FuzzyDateParser(
      ParseAttempt.TRY_ISO_INSTANT,
      ParseAttempt.TRY_ISO_LOCAL_DATE);

  /**
   * Converts the provided {@link Instant ISO instant} to a {@link LocalDate}. Can be
   * used to generate SQL WHERE clauses for {@link OaiRequest#getFrom()
   * OaiRequest.getFrom} and {@link OaiRequest#getUntil() OaiRequest.getUntil}.
   *
   * @param oaiDate
   * @return
   */
  public static LocalDate toLocalDate(Instant oaiDate) {
    return LocalDate.ofInstant(oaiDate, UTC);
  }

  /**
   * Converts the provided {@link Instant ISO instant} to a {@link LocalDateTime}.
   * Can be used to generate SQL WHERE clauses for {@link OaiRequest#getFrom()
   * OaiRequest.getFrom} and {@link OaiRequest#getUntil() OaiRequest.getUntil}.
   *
   * @param oaiDate
   * @return
   */
  public static LocalDateTime toLocalDateTime(Instant oaiDate) {
    return LocalDateTime.ofInstant(oaiDate, UTC);
  }

  /**
   * Formats the provided {@link LocalDate} instance as an ISO instant (like
   * "2017-08-14"). Can be used to insert dates into the OAI-PMH XML output.
   *
   * @param nativeDate
   * @return
   */
  public static String print(LocalDate nativeDate) {
    return ISO_LOCAL_DATE.format(nativeDate);
  }

  /**
   * Formats the provided {@link LocalDateTime} instance as an ISO instant (like
   * "2017-08-14T17:43:56Z"). Can be used to insert dates into the OAI-PMH XML
   * output.
   *
   * @param nativeDate
   * @return
   */
  public static String print(LocalDateTime nativeDate) {
    return ISO_INSTANT.format(nativeDate.toInstant(UTC));
  }

  /**
   * Used to parse the "from" and "until" parameters into an into an instance of
   * {@link Instant}.
   *
   * @param dateString
   * @return
   * @throws OaiProtocolException
   */
  public FuzzyDate parse(String dateString) throws OaiProtocolException {
    try {
      return parser.parse(dateString);
    } catch (FuzzyDateException e) {
      throw new OaiProtocolException(BadArgumentError.badDate(dateString));
    }
  }

}

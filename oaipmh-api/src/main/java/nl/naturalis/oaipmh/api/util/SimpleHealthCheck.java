package nl.naturalis.oaipmh.api.util;

/**
 * Allows repositories to define and provide health checks without requiring a (possibly
 * otherwise useless) dependency on the DropWizard framework. The
 * <code>SimplyHealthCheck</code> instances provided by the repository are converted to
 * Dropwizard <code>HealthCheck</code> instances and then registered with the OAIPMH
 * server.
 *
 * @author Ayco Holleman
 */
public interface SimpleHealthCheck {

  /**
   * Returns the name or description of what is being checked.
   *
   * @return
   */
  String getName();

  /**
   * Whether or not the check turned out OK.
   *
   * @return
   */
  boolean isHealthy();

  /**
   * An optional message provided along with healty/unhealthy signal. The default
   * implementation returns <code>null</code>.
   *
   * @return
   */
  default String getMessage() {
    return null;
  }

  /**
   * An optional <code>Exception</code> provided along with the unhealthy signal. The
   * default implementation returns <code>null</code>.
   *
   * @return
   */
  default Throwable getError() {
    return null;
  }

}

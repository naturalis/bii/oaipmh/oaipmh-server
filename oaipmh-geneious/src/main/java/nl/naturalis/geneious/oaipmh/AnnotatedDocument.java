package nl.naturalis.geneious.oaipmh;

import nl.naturalis.geneious.oaipmh.util.GeneiousOaiUtil;

import java.time.Instant;
import java.time.LocalDate;

import static nl.naturalis.geneious.oaipmh.HiddenFields.HiddenField.cache_name;
import static nl.naturalis.geneious.oaipmh.HiddenFields.HiddenField.override_cache_name;

/**
 * Models the annotated_document table in the Geneious database.
 *
 * @author Ayco Holleman
 */
public final class AnnotatedDocument {

  /*
   * This is, in fact, metadata information about the annotated_document record, namely
   * whether it must be discarded when generating an OAI-PMH response.
   */
  private boolean discard;

  // Columns in the SQL query
  private int id;
  private LocalDate modified;

  // The raw contents of the document_xml and plugin_document_xml columns
  private String documentXmlString;
  private String pluginDocumentXmlString;

  // Deserializations of the document_xml and plugin_document_xml columns
  private DocumentXml document;
  private PluginDocumentXml pluginDocumentXml;

  // The "value" column in string_search_field_value table
  private String extractId;

  // Calculated fields
  private String name;
  private Maturity maturity;
  // The override_modified_date within the document_xml column if it is present, or the value of the "value" column of the
  // date_search_field_value table. The latter is truncated to whole days while the former is down to the second.
  private Instant highResModifiedDate;

  public void discard() {
    this.discard = true;
  }

  public boolean isDiscarded() {
    return discard;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj == null || obj.getClass() != AnnotatedDocument.class) {
      return false;
    }
    return id == ((AnnotatedDocument) obj).id;
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "[" + id + "]";
  }

  @SuppressWarnings("unchecked")
  public <T extends PluginDocumentXml> T getPluginDocument() {
    return (T) getPluginDocumentXml();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public LocalDate getModified() {
    return modified;
  }

  public void setModified(LocalDate modified) {
    this.modified = modified;
  }

  public String getDocumentXmlString() {
    return documentXmlString;
  }

  public void setDocumentXmlString(String documentXmlString) {
    this.documentXmlString = documentXmlString;
  }

  public String getPluginDocumentXmlString() {
    return pluginDocumentXmlString;
  }

  public void setPluginDocumentXmlString(String pluginDocumentXmlString) {
    this.pluginDocumentXmlString = pluginDocumentXmlString;
  }

  public String getExtractId() {
    return extractId;
  }

  public void setExtractId(String extractId) {
    this.extractId = extractId;
  }

  public DocumentXml getDocumentXml() {
    return document;
  }

  public void setDocumentXml(DocumentXml documentXml) {
    this.document = documentXml;
  }

  public PluginDocumentXml getPluginDocumentXml() {
    return pluginDocumentXml;
  }

  public void setPluginDocumentXml(PluginDocumentXml pluginDocumentXml) {
    this.pluginDocumentXml = pluginDocumentXml;
  }

  public String getName() {
    if (name == null) {
      String s = getDocumentXml().getHiddenField(override_cache_name);
      if (s == null) {
        s = getDocumentXml().getHiddenField(cache_name);
        if (s == null && getPluginDocumentXml().getClass() == XmlSerialisableRootElement.class) {
          s = ((XmlSerialisableRootElement) getPluginDocumentXml()).getName();
        }
      }
      name = s == null ? "<name unknown>" : s;
    }
    return name;
  }

  public Maturity getMaturity() {
    if (maturity == null) {
      maturity = GeneiousOaiUtil.getMaturity(this);
    }
    return maturity;
  }

  public Instant getHighResModifiedDate() {
    return highResModifiedDate;
  }

  public void setHighResModifiedDate(Instant highResModifiedDate) {
    this.highResModifiedDate = highResModifiedDate;
  }

}

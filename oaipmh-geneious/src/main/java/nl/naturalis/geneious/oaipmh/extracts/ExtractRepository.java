package nl.naturalis.geneious.oaipmh.extracts;

import nl.naturalis.geneious.oaipmh.GeneiousOaiRepository;
import nl.naturalis.geneious.oaipmh.GeneiousRepositoryConfig;
import nl.naturalis.geneious.oaipmh.ListRecordsHandler;

/**
 * OAI repository for DNA extracts.
 * 
 * @author Ayco Holleman
 *
 */
public final class ExtractRepository extends GeneiousOaiRepository {

  public ExtractRepository(GeneiousRepositoryConfig config) {
    super(config);
  }

  @Override
  protected ListRecordsHandler newListRecordsHandler(GeneiousRepositoryConfig config) {
    return new ExtractListRecordsHandler(config);
  }

}

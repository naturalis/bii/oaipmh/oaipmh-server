package nl.naturalis.oaipmh.api.util;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

import nl.naturalis.fuzzydate.FuzzyDate;
import nl.naturalis.oaipmh.api.OaiProtocolException;

public class OaiDateParserTest {

  @Test
  public void parse_1() throws OaiProtocolException {
    String s = "2017-03-12T14:08:27Z";
    FuzzyDate fd = OaiDateParser.INSTANCE.parse(s);
    assertEquals(DateTimeFormatter.ISO_INSTANT, fd.getParseAttempt().getFormatter());
  }

  @Test
  public void parse_2() throws OaiProtocolException {
    String s = "2017-03-12";
    FuzzyDate fd = OaiDateParser.INSTANCE.parse(s);
    assertEquals(DateTimeFormatter.ISO_LOCAL_DATE, fd.getParseAttempt().getFormatter());
  }

  @Test(expected = OaiProtocolException.class)
  public void parse_3() throws OaiProtocolException {
    String s = "2017-03-12 14:08:27";
    OaiDateParser.INSTANCE.parse(s);
  }

  @Test
  public void print_1() {
    LocalDateTime date = LocalDateTime.of(2017, 8, 14, 9, 33);
    assertEquals("2017-08-14T09:33:00Z", OaiDateParser.print(date));
  }

  @Test
  public void print_2() {
    LocalDate date = LocalDate.of(2017, 8, 14);
    assertEquals("2017-08-14", OaiDateParser.print(date));
  }

}

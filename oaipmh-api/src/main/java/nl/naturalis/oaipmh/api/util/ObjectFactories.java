package nl.naturalis.oaipmh.api.util;

/**
 * Provides JAXB object factories for various XML schemas, enabling classes that use them
 * to avoid name clashes.
 *
 * @author Ayco Holleman
 */
public class ObjectFactories {

  public static final org.openarchives.oai._2.ObjectFactory OAI;
  public static final org.openarchives.oai._2_0.oai_dc.ObjectFactory OAI_DC;
  public static final org.purl.dc.elements._1.ObjectFactory DC;
  public static final org.tdwg.abcd2.ObjectFactory ABCD2;
  public static final org.tdwg.ggbn.ObjectFactory GGBN;

  static {
    OAI = new org.openarchives.oai._2.ObjectFactory();
    OAI_DC = new org.openarchives.oai._2_0.oai_dc.ObjectFactory();
    DC = new org.purl.dc.elements._1.ObjectFactory();
    ABCD2 = new org.tdwg.abcd2.ObjectFactory();
    GGBN = new org.tdwg.ggbn.ObjectFactory();
  }

  private ObjectFactories() { }

}

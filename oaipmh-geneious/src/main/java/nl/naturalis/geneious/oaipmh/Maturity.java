package nl.naturalis.geneious.oaipmh;

public enum Maturity {

  UNKNOWN,
  DUMMY,
  AB1_REVERSED ("ab1 (reversed)"),
  TRACE("ab1/scf"),
  FASTA,
  CONTIG,
  CONSENSUS;

  private final String name;

  private Maturity() {
    this.name = name().toLowerCase();
  }

  private Maturity(String name) {
    this.name = name;
  }

  public boolean isHigherThan(Maturity other) {
    return this.ordinal() > other.ordinal();
  }

  public String toString() {
    return name;
  }

}

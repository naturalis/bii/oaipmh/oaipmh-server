//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.2 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.02.07 at 09:07:12 PM CET 
//


package org.tdwg.abcd2;

import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tdwg.abcd2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tdwg.abcd2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataSets }
     * 
     */
    public DataSets createDataSets() {
        return new DataSets();
    }

    /**
     * Create an instance of {@link ScientificName }
     * 
     */
    public ScientificName createScientificName() {
        return new ScientificName();
    }

    /**
     * Create an instance of {@link ScientificNameIdentified }
     * 
     */
    public ScientificNameIdentified createScientificNameIdentified() {
        return new ScientificNameIdentified();
    }

    /**
     * Create an instance of {@link NameBotanical }
     * 
     */
    public NameBotanical createNameBotanical() {
        return new NameBotanical();
    }

    /**
     * Create an instance of {@link TaxonIdentified }
     * 
     */
    public TaxonIdentified createTaxonIdentified() {
        return new TaxonIdentified();
    }

    /**
     * Create an instance of {@link ContentMetadata }
     * 
     */
    public ContentMetadata createContentMetadata() {
        return new ContentMetadata();
    }

    /**
     * Create an instance of {@link ContentMetadata.Scope }
     * 
     */
    public ContentMetadata.Scope createContentMetadataScope() {
        return new ContentMetadata.Scope();
    }

    /**
     * Create an instance of {@link MeasurementOrFact }
     * 
     */
    public MeasurementOrFact createMeasurementOrFact() {
        return new MeasurementOrFact();
    }

    /**
     * Create an instance of {@link ZoologicalUnit }
     * 
     */
    public ZoologicalUnit createZoologicalUnit() {
        return new ZoologicalUnit();
    }

    /**
     * Create an instance of {@link Stratigraphy }
     * 
     */
    public Stratigraphy createStratigraphy() {
        return new Stratigraphy();
    }

    /**
     * Create an instance of {@link PaleontologicalUnit }
     * 
     */
    public PaleontologicalUnit createPaleontologicalUnit() {
        return new PaleontologicalUnit();
    }

    /**
     * Create an instance of {@link PGRUnit }
     * 
     */
    public PGRUnit createPGRUnit() {
        return new PGRUnit();
    }

    /**
     * Create an instance of {@link MycologicalUnit }
     * 
     */
    public MycologicalUnit createMycologicalUnit() {
        return new MycologicalUnit();
    }

    /**
     * Create an instance of {@link GrowthConditionAtomized }
     * 
     */
    public GrowthConditionAtomized createGrowthConditionAtomized() {
        return new GrowthConditionAtomized();
    }

    /**
     * Create an instance of {@link CultureCollectionUnit }
     * 
     */
    public CultureCollectionUnit createCultureCollectionUnit() {
        return new CultureCollectionUnit();
    }

    /**
     * Create an instance of {@link PersonName }
     * 
     */
    public PersonName createPersonName() {
        return new PersonName();
    }

    /**
     * Create an instance of {@link Label }
     * 
     */
    public Label createLabel() {
        return new Label();
    }

    /**
     * Create an instance of {@link Organization }
     * 
     */
    public Organization createOrganization() {
        return new Organization();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link Reference }
     * 
     */
    public Reference createReference() {
        return new Reference();
    }

    /**
     * Create an instance of {@link MultimediaObject }
     * 
     */
    public MultimediaObject createMultimediaObject() {
        return new MultimediaObject();
    }

    /**
     * Create an instance of {@link LegalStatements }
     * 
     */
    public LegalStatements createLegalStatements() {
        return new LegalStatements();
    }

    /**
     * Create an instance of {@link Unit }
     * 
     */
    public Unit createUnit() {
        return new Unit();
    }

    /**
     * Create an instance of {@link Unit.Annotations }
     * 
     */
    public Unit.Annotations createUnitAnnotations() {
        return new Unit.Annotations();
    }

    /**
     * Create an instance of {@link Unit.FieldNumbers }
     * 
     */
    public Unit.FieldNumbers createUnitFieldNumbers() {
        return new Unit.FieldNumbers();
    }

    /**
     * Create an instance of {@link Unit.Assemblages }
     * 
     */
    public Unit.Assemblages createUnitAssemblages() {
        return new Unit.Assemblages();
    }

    /**
     * Create an instance of {@link Unit.Assemblages.Assemblage }
     * 
     */
    public Unit.Assemblages.Assemblage createUnitAssemblagesAssemblage() {
        return new Unit.Assemblages.Assemblage();
    }

    /**
     * Create an instance of {@link Unit.Associations }
     * 
     */
    public Unit.Associations createUnitAssociations() {
        return new Unit.Associations();
    }

    /**
     * Create an instance of {@link Unit.Associations.Association }
     * 
     */
    public Unit.Associations.Association createUnitAssociationsAssociation() {
        return new Unit.Associations.Association();
    }

    /**
     * Create an instance of {@link Unit.ObservationUnit }
     * 
     */
    public Unit.ObservationUnit createUnitObservationUnit() {
        return new Unit.ObservationUnit();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit }
     * 
     */
    public Unit.SpecimenUnit createUnitSpecimenUnit() {
        return new Unit.SpecimenUnit();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.NomenclaturalTypeDesignations }
     * 
     */
    public Unit.SpecimenUnit.NomenclaturalTypeDesignations createUnitSpecimenUnitNomenclaturalTypeDesignations() {
        return new Unit.SpecimenUnit.NomenclaturalTypeDesignations();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.PreviousUnits }
     * 
     */
    public Unit.SpecimenUnit.PreviousUnits createUnitSpecimenUnitPreviousUnits() {
        return new Unit.SpecimenUnit.PreviousUnits();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Marks }
     * 
     */
    public Unit.SpecimenUnit.Marks createUnitSpecimenUnitMarks() {
        return new Unit.SpecimenUnit.Marks();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Marks.Mark }
     * 
     */
    public Unit.SpecimenUnit.Marks.Mark createUnitSpecimenUnitMarksMark() {
        return new Unit.SpecimenUnit.Marks.Mark();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Preservations }
     * 
     */
    public Unit.SpecimenUnit.Preservations createUnitSpecimenUnitPreservations() {
        return new Unit.SpecimenUnit.Preservations();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Preparations }
     * 
     */
    public Unit.SpecimenUnit.Preparations createUnitSpecimenUnitPreparations() {
        return new Unit.SpecimenUnit.Preparations();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Preparations.Preparation }
     * 
     */
    public Unit.SpecimenUnit.Preparations.Preparation createUnitSpecimenUnitPreparationsPreparation() {
        return new Unit.SpecimenUnit.Preparations.Preparation();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Accessions }
     * 
     */
    public Unit.SpecimenUnit.Accessions createUnitSpecimenUnitAccessions() {
        return new Unit.SpecimenUnit.Accessions();
    }

    /**
     * Create an instance of {@link Unit.ContentContacts }
     * 
     */
    public Unit.ContentContacts createUnitContentContacts() {
        return new Unit.ContentContacts();
    }

    /**
     * Create an instance of {@link Identification }
     * 
     */
    public Identification createIdentification() {
        return new Identification();
    }

    /**
     * Create an instance of {@link Gathering }
     * 
     */
    public Gathering createGathering() {
        return new Gathering();
    }

    /**
     * Create an instance of {@link Gathering.Synecology }
     * 
     */
    public Gathering.Synecology createGatheringSynecology() {
        return new Gathering.Synecology();
    }

    /**
     * Create an instance of {@link Gathering.Biotope }
     * 
     */
    public Gathering.Biotope createGatheringBiotope() {
        return new Gathering.Biotope();
    }

    /**
     * Create an instance of {@link Gathering.CoordinateSets }
     * 
     */
    public Gathering.CoordinateSets createGatheringCoordinateSets() {
        return new Gathering.CoordinateSets();
    }

    /**
     * Create an instance of {@link Gathering.CoordinateSets.CoordinateSet }
     * 
     */
    public Gathering.CoordinateSets.CoordinateSet createGatheringCoordinateSetsCoordinateSet() {
        return new Gathering.CoordinateSets.CoordinateSet();
    }

    /**
     * Create an instance of {@link Gathering.NamedPlaceRelations }
     * 
     */
    public Gathering.NamedPlaceRelations createGatheringNamedPlaceRelations() {
        return new Gathering.NamedPlaceRelations();
    }

    /**
     * Create an instance of {@link Gathering.NamedAreas }
     * 
     */
    public Gathering.NamedAreas createGatheringNamedAreas() {
        return new Gathering.NamedAreas();
    }

    /**
     * Create an instance of {@link Gathering.Project }
     * 
     */
    public Gathering.Project createGatheringProject() {
        return new Gathering.Project();
    }

    /**
     * Create an instance of {@link Gathering.GatheringAgents }
     * 
     */
    public Gathering.GatheringAgents createGatheringGatheringAgents() {
        return new Gathering.GatheringAgents();
    }

    /**
     * Create an instance of {@link DataSets.DataSet }
     * 
     */
    public DataSets.DataSet createDataSetsDataSet() {
        return new DataSets.DataSet();
    }

    /**
     * Create an instance of {@link org.tdwg.abcd2.NamedArea }
     * 
     */
    public org.tdwg.abcd2.NamedArea createNamedArea() {
        return new org.tdwg.abcd2.NamedArea();
    }

    /**
     * Create an instance of {@link Sequence }
     * 
     */
    public Sequence createSequence() {
        return new Sequence();
    }

    /**
     * Create an instance of {@link StringL }
     * 
     */
    public StringL createStringL() {
        return new StringL();
    }

    /**
     * Create an instance of {@link StringL255 }
     * 
     */
    public StringL255 createStringL255() {
        return new StringL255();
    }

    /**
     * Create an instance of {@link StringP }
     * 
     */
    public StringP createStringP() {
        return new StringP();
    }

    /**
     * Create an instance of {@link StringP255 }
     * 
     */
    public StringP255 createStringP255() {
        return new StringP255();
    }

    /**
     * Create an instance of {@link StringLP }
     * 
     */
    public StringLP createStringLP() {
        return new StringLP();
    }

    /**
     * Create an instance of {@link StringLP255 }
     * 
     */
    public StringLP255 createStringLP255() {
        return new StringLP255();
    }

    /**
     * Create an instance of {@link AnyUriP }
     * 
     */
    public AnyUriP createAnyUriP() {
        return new AnyUriP();
    }

    /**
     * Create an instance of {@link DateTime }
     * 
     */
    public DateTime createDateTime() {
        return new DateTime();
    }

    /**
     * Create an instance of {@link Temperature }
     * 
     */
    public Temperature createTemperature() {
        return new Temperature();
    }

    /**
     * Create an instance of {@link LegalStatement }
     * 
     */
    public LegalStatement createLegalStatement() {
        return new LegalStatement();
    }

    /**
     * Create an instance of {@link ImageSize }
     * 
     */
    public ImageSize createImageSize() {
        return new ImageSize();
    }

    /**
     * Create an instance of {@link ContactP }
     * 
     */
    public ContactP createContactP() {
        return new ContactP();
    }

    /**
     * Create an instance of {@link LabelRepr }
     * 
     */
    public LabelRepr createLabelRepr() {
        return new LabelRepr();
    }

    /**
     * Create an instance of {@link BotanicalGardenUnit }
     * 
     */
    public BotanicalGardenUnit createBotanicalGardenUnit() {
        return new BotanicalGardenUnit();
    }

    /**
     * Create an instance of {@link HerbariumUnit }
     * 
     */
    public HerbariumUnit createHerbariumUnit() {
        return new HerbariumUnit();
    }

    /**
     * Create an instance of {@link Country }
     * 
     */
    public Country createCountry() {
        return new Country();
    }

    /**
     * Create an instance of {@link StratigraphicTerm }
     * 
     */
    public StratigraphicTerm createStratigraphicTerm() {
        return new StratigraphicTerm();
    }

    /**
     * Create an instance of {@link StratigraphicTermL }
     * 
     */
    public StratigraphicTermL createStratigraphicTermL() {
        return new StratigraphicTermL();
    }

    /**
     * Create an instance of {@link MetadataDescriptionRepr }
     * 
     */
    public MetadataDescriptionRepr createMetadataDescriptionRepr() {
        return new MetadataDescriptionRepr();
    }

    /**
     * Create an instance of {@link RevisionData }
     * 
     */
    public RevisionData createRevisionData() {
        return new RevisionData();
    }

    /**
     * Create an instance of {@link NameBacterial }
     * 
     */
    public NameBacterial createNameBacterial() {
        return new NameBacterial();
    }

    /**
     * Create an instance of {@link NameViral }
     * 
     */
    public NameViral createNameViral() {
        return new NameViral();
    }

    /**
     * Create an instance of {@link NameZoological }
     * 
     */
    public NameZoological createNameZoological() {
        return new NameZoological();
    }

    /**
     * Create an instance of {@link HigherTaxon }
     * 
     */
    public HigherTaxon createHigherTaxon() {
        return new HigherTaxon();
    }

    /**
     * Create an instance of {@link Permit }
     * 
     */
    public Permit createPermit() {
        return new Permit();
    }

    /**
     * Create an instance of {@link ScientificName.NameAtomized }
     * 
     */
    public ScientificName.NameAtomized createScientificNameNameAtomized() {
        return new ScientificName.NameAtomized();
    }

    /**
     * Create an instance of {@link ScientificNameIdentified.IdentificationQualifier }
     * 
     */
    public ScientificNameIdentified.IdentificationQualifier createScientificNameIdentifiedIdentificationQualifier() {
        return new ScientificNameIdentified.IdentificationQualifier();
    }

    /**
     * Create an instance of {@link NameBotanical.HybridFlag }
     * 
     */
    public NameBotanical.HybridFlag createNameBotanicalHybridFlag() {
        return new NameBotanical.HybridFlag();
    }

    /**
     * Create an instance of {@link NameBotanical.TradeDesignationNames }
     * 
     */
    public NameBotanical.TradeDesignationNames createNameBotanicalTradeDesignationNames() {
        return new NameBotanical.TradeDesignationNames();
    }

    /**
     * Create an instance of {@link TaxonIdentified.HigherTaxa }
     * 
     */
    public TaxonIdentified.HigherTaxa createTaxonIdentifiedHigherTaxa() {
        return new TaxonIdentified.HigherTaxa();
    }

    /**
     * Create an instance of {@link ContentMetadata.Description }
     * 
     */
    public ContentMetadata.Description createContentMetadataDescription() {
        return new ContentMetadata.Description();
    }

    /**
     * Create an instance of {@link ContentMetadata.Version }
     * 
     */
    public ContentMetadata.Version createContentMetadataVersion() {
        return new ContentMetadata.Version();
    }

    /**
     * Create an instance of {@link ContentMetadata.Owners }
     * 
     */
    public ContentMetadata.Owners createContentMetadataOwners() {
        return new ContentMetadata.Owners();
    }

    /**
     * Create an instance of {@link ContentMetadata.Scope.GeoecologicalTerms }
     * 
     */
    public ContentMetadata.Scope.GeoecologicalTerms createContentMetadataScopeGeoecologicalTerms() {
        return new ContentMetadata.Scope.GeoecologicalTerms();
    }

    /**
     * Create an instance of {@link ContentMetadata.Scope.TaxonomicTerms }
     * 
     */
    public ContentMetadata.Scope.TaxonomicTerms createContentMetadataScopeTaxonomicTerms() {
        return new ContentMetadata.Scope.TaxonomicTerms();
    }

    /**
     * Create an instance of {@link MeasurementOrFact.MeasurementOrFactAtomized }
     * 
     */
    public MeasurementOrFact.MeasurementOrFactAtomized createMeasurementOrFactMeasurementOrFactAtomized() {
        return new MeasurementOrFact.MeasurementOrFactAtomized();
    }

    /**
     * Create an instance of {@link ZoologicalUnit.PhasesOrStages }
     * 
     */
    public ZoologicalUnit.PhasesOrStages createZoologicalUnitPhasesOrStages() {
        return new ZoologicalUnit.PhasesOrStages();
    }

    /**
     * Create an instance of {@link Stratigraphy.ChronostratigraphicTerms }
     * 
     */
    public Stratigraphy.ChronostratigraphicTerms createStratigraphyChronostratigraphicTerms() {
        return new Stratigraphy.ChronostratigraphicTerms();
    }

    /**
     * Create an instance of {@link Stratigraphy.BiostratigraphicTerms }
     * 
     */
    public Stratigraphy.BiostratigraphicTerms createStratigraphyBiostratigraphicTerms() {
        return new Stratigraphy.BiostratigraphicTerms();
    }

    /**
     * Create an instance of {@link Stratigraphy.LithostratigraphicTerms }
     * 
     */
    public Stratigraphy.LithostratigraphicTerms createStratigraphyLithostratigraphicTerms() {
        return new Stratigraphy.LithostratigraphicTerms();
    }

    /**
     * Create an instance of {@link PaleontologicalUnit.Preservation }
     * 
     */
    public PaleontologicalUnit.Preservation createPaleontologicalUnitPreservation() {
        return new PaleontologicalUnit.Preservation();
    }

    /**
     * Create an instance of {@link PGRUnit.AccessionNames }
     * 
     */
    public PGRUnit.AccessionNames createPGRUnitAccessionNames() {
        return new PGRUnit.AccessionNames();
    }

    /**
     * Create an instance of {@link MycologicalUnit.MycologicalLiveStages }
     * 
     */
    public MycologicalUnit.MycologicalLiveStages createMycologicalUnitMycologicalLiveStages() {
        return new MycologicalUnit.MycologicalLiveStages();
    }

    /**
     * Create an instance of {@link GrowthConditionAtomized.References }
     * 
     */
    public GrowthConditionAtomized.References createGrowthConditionAtomizedReferences() {
        return new GrowthConditionAtomized.References();
    }

    /**
     * Create an instance of {@link GrowthConditionAtomized.GrowthConditionsMeasurementsOrFacts }
     * 
     */
    public GrowthConditionAtomized.GrowthConditionsMeasurementsOrFacts createGrowthConditionAtomizedGrowthConditionsMeasurementsOrFacts() {
        return new GrowthConditionAtomized.GrowthConditionsMeasurementsOrFacts();
    }

    /**
     * Create an instance of {@link CultureCollectionUnit.CultureNames }
     * 
     */
    public CultureCollectionUnit.CultureNames createCultureCollectionUnitCultureNames() {
        return new CultureCollectionUnit.CultureNames();
    }

    /**
     * Create an instance of {@link CultureCollectionUnit.GrowthConditionsAtomized }
     * 
     */
    public CultureCollectionUnit.GrowthConditionsAtomized createCultureCollectionUnitGrowthConditionsAtomized() {
        return new CultureCollectionUnit.GrowthConditionsAtomized();
    }

    /**
     * Create an instance of {@link CultureCollectionUnit.References }
     * 
     */
    public CultureCollectionUnit.References createCultureCollectionUnitReferences() {
        return new CultureCollectionUnit.References();
    }

    /**
     * Create an instance of {@link PersonName.AtomizedName }
     * 
     */
    public PersonName.AtomizedName createPersonNameAtomizedName() {
        return new PersonName.AtomizedName();
    }

    /**
     * Create an instance of {@link Label.Representation }
     * 
     */
    public Label.Representation createLabelRepresentation() {
        return new Label.Representation();
    }

    /**
     * Create an instance of {@link Organization.Divisions }
     * 
     */
    public Organization.Divisions createOrganizationDivisions() {
        return new Organization.Divisions();
    }

    /**
     * Create an instance of {@link Contact.Roles }
     * 
     */
    public Contact.Roles createContactRoles() {
        return new Contact.Roles();
    }

    /**
     * Create an instance of {@link Contact.Addresses }
     * 
     */
    public Contact.Addresses createContactAddresses() {
        return new Contact.Addresses();
    }

    /**
     * Create an instance of {@link Contact.TelephoneNumbers }
     * 
     */
    public Contact.TelephoneNumbers createContactTelephoneNumbers() {
        return new Contact.TelephoneNumbers();
    }

    /**
     * Create an instance of {@link Contact.EmailAddresses }
     * 
     */
    public Contact.EmailAddresses createContactEmailAddresses() {
        return new Contact.EmailAddresses();
    }

    /**
     * Create an instance of {@link Contact.ResourceURIs }
     * 
     */
    public Contact.ResourceURIs createContactResourceURIs() {
        return new Contact.ResourceURIs();
    }

    /**
     * Create an instance of {@link Reference.ResourceURIs }
     * 
     */
    public Reference.ResourceURIs createReferenceResourceURIs() {
        return new Reference.ResourceURIs();
    }

    /**
     * Create an instance of {@link MultimediaObject.Tags }
     * 
     */
    public MultimediaObject.Tags createMultimediaObjectTags() {
        return new MultimediaObject.Tags();
    }

    /**
     * Create an instance of {@link MultimediaObject.Ratings }
     * 
     */
    public MultimediaObject.Ratings createMultimediaObjectRatings() {
        return new MultimediaObject.Ratings();
    }

    /**
     * Create an instance of {@link MultimediaObject.Creators }
     * 
     */
    public MultimediaObject.Creators createMultimediaObjectCreators() {
        return new MultimediaObject.Creators();
    }

    /**
     * Create an instance of {@link MultimediaObject.TaxaInBackground }
     * 
     */
    public MultimediaObject.TaxaInBackground createMultimediaObjectTaxaInBackground() {
        return new MultimediaObject.TaxaInBackground();
    }

    /**
     * Create an instance of {@link MultimediaObject.Audio }
     * 
     */
    public MultimediaObject.Audio createMultimediaObjectAudio() {
        return new MultimediaObject.Audio();
    }

    /**
     * Create an instance of {@link MultimediaObject.Video }
     * 
     */
    public MultimediaObject.Video createMultimediaObjectVideo() {
        return new MultimediaObject.Video();
    }

    /**
     * Create an instance of {@link MultimediaObject.Image }
     * 
     */
    public MultimediaObject.Image createMultimediaObjectImage() {
        return new MultimediaObject.Image();
    }

    /**
     * Create an instance of {@link LegalStatements.Licenses }
     * 
     */
    public LegalStatements.Licenses createLegalStatementsLicenses() {
        return new LegalStatements.Licenses();
    }

    /**
     * Create an instance of {@link LegalStatements.Acknowledgements }
     * 
     */
    public LegalStatements.Acknowledgements createLegalStatementsAcknowledgements() {
        return new LegalStatements.Acknowledgements();
    }

    /**
     * Create an instance of {@link LegalStatements.SuggestedCitations }
     * 
     */
    public LegalStatements.SuggestedCitations createLegalStatementsSuggestedCitations() {
        return new LegalStatements.SuggestedCitations();
    }

    /**
     * Create an instance of {@link LegalStatements.OtherLegalStatements }
     * 
     */
    public LegalStatements.OtherLegalStatements createLegalStatementsOtherLegalStatements() {
        return new LegalStatements.OtherLegalStatements();
    }

    /**
     * Create an instance of {@link Unit.UnitReferences }
     * 
     */
    public Unit.UnitReferences createUnitUnitReferences() {
        return new Unit.UnitReferences();
    }

    /**
     * Create an instance of {@link Unit.Identifications }
     * 
     */
    public Unit.Identifications createUnitIdentifications() {
        return new Unit.Identifications();
    }

    /**
     * Create an instance of {@link Unit.MultimediaObjects }
     * 
     */
    public Unit.MultimediaObjects createUnitMultimediaObjects() {
        return new Unit.MultimediaObjects();
    }

    /**
     * Create an instance of {@link Unit.NamedCollectionsOrSurveys }
     * 
     */
    public Unit.NamedCollectionsOrSurveys createUnitNamedCollectionsOrSurveys() {
        return new Unit.NamedCollectionsOrSurveys();
    }

    /**
     * Create an instance of {@link Unit.FieldNotesReferences }
     * 
     */
    public Unit.FieldNotesReferences createUnitFieldNotesReferences() {
        return new Unit.FieldNotesReferences();
    }

    /**
     * Create an instance of {@link Unit.MeasurementsOrFacts }
     * 
     */
    public Unit.MeasurementsOrFacts createUnitMeasurementsOrFacts() {
        return new Unit.MeasurementsOrFacts();
    }

    /**
     * Create an instance of {@link Unit.Sequences }
     * 
     */
    public Unit.Sequences createUnitSequences() {
        return new Unit.Sequences();
    }

    /**
     * Create an instance of {@link Unit.UnitExtensions }
     * 
     */
    public Unit.UnitExtensions createUnitUnitExtensions() {
        return new Unit.UnitExtensions();
    }

    /**
     * Create an instance of {@link Unit.ResourceURIs }
     * 
     */
    public Unit.ResourceURIs createUnitResourceURIs() {
        return new Unit.ResourceURIs();
    }

    /**
     * Create an instance of {@link Unit.Annotations.Annotation }
     * 
     */
    public Unit.Annotations.Annotation createUnitAnnotationsAnnotation() {
        return new Unit.Annotations.Annotation();
    }

    /**
     * Create an instance of {@link Unit.FieldNumbers.FieldNumber }
     * 
     */
    public Unit.FieldNumbers.FieldNumber createUnitFieldNumbersFieldNumber() {
        return new Unit.FieldNumbers.FieldNumber();
    }

    /**
     * Create an instance of {@link Unit.Assemblages.Assemblage.ResourceURIs }
     * 
     */
    public Unit.Assemblages.Assemblage.ResourceURIs createUnitAssemblagesAssemblageResourceURIs() {
        return new Unit.Assemblages.Assemblage.ResourceURIs();
    }

    /**
     * Create an instance of {@link Unit.Associations.Association.ResourceURIs }
     * 
     */
    public Unit.Associations.Association.ResourceURIs createUnitAssociationsAssociationResourceURIs() {
        return new Unit.Associations.Association.ResourceURIs();
    }

    /**
     * Create an instance of {@link Unit.ObservationUnit.ObservationUnitIDs }
     * 
     */
    public Unit.ObservationUnit.ObservationUnitIDs createUnitObservationUnitObservationUnitIDs() {
        return new Unit.ObservationUnit.ObservationUnitIDs();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.LoanRestrictions }
     * 
     */
    public Unit.SpecimenUnit.LoanRestrictions createUnitSpecimenUnitLoanRestrictions() {
        return new Unit.SpecimenUnit.LoanRestrictions();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Acquisition }
     * 
     */
    public Unit.SpecimenUnit.Acquisition createUnitSpecimenUnitAcquisition() {
        return new Unit.SpecimenUnit.Acquisition();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.SpecimenMeasurementsOrFacts }
     * 
     */
    public Unit.SpecimenUnit.SpecimenMeasurementsOrFacts createUnitSpecimenUnitSpecimenMeasurementsOrFacts() {
        return new Unit.SpecimenUnit.SpecimenMeasurementsOrFacts();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.NomenclaturalTypeDesignations.NomenclaturalTypeDesignation }
     * 
     */
    public Unit.SpecimenUnit.NomenclaturalTypeDesignations.NomenclaturalTypeDesignation createUnitSpecimenUnitNomenclaturalTypeDesignationsNomenclaturalTypeDesignation() {
        return new Unit.SpecimenUnit.NomenclaturalTypeDesignations.NomenclaturalTypeDesignation();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.PreviousUnits.PreviousUnit }
     * 
     */
    public Unit.SpecimenUnit.PreviousUnits.PreviousUnit createUnitSpecimenUnitPreviousUnitsPreviousUnit() {
        return new Unit.SpecimenUnit.PreviousUnits.PreviousUnit();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Marks.Mark.Images }
     * 
     */
    public Unit.SpecimenUnit.Marks.Mark.Images createUnitSpecimenUnitMarksMarkImages() {
        return new Unit.SpecimenUnit.Marks.Mark.Images();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Preservations.Preservation }
     * 
     */
    public Unit.SpecimenUnit.Preservations.Preservation createUnitSpecimenUnitPreservationsPreservation() {
        return new Unit.SpecimenUnit.Preservations.Preservation();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Preparations.Preparation.SampleDesignations }
     * 
     */
    public Unit.SpecimenUnit.Preparations.Preparation.SampleDesignations createUnitSpecimenUnitPreparationsPreparationSampleDesignations() {
        return new Unit.SpecimenUnit.Preparations.Preparation.SampleDesignations();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Preparations.Preparation.References }
     * 
     */
    public Unit.SpecimenUnit.Preparations.Preparation.References createUnitSpecimenUnitPreparationsPreparationReferences() {
        return new Unit.SpecimenUnit.Preparations.Preparation.References();
    }

    /**
     * Create an instance of {@link Unit.SpecimenUnit.Accessions.Accession }
     * 
     */
    public Unit.SpecimenUnit.Accessions.Accession createUnitSpecimenUnitAccessionsAccession() {
        return new Unit.SpecimenUnit.Accessions.Accession();
    }

    /**
     * Create an instance of {@link Unit.ContentContacts.ContentContact }
     * 
     */
    public Unit.ContentContacts.ContentContact createUnitContentContactsContentContact() {
        return new Unit.ContentContacts.ContentContact();
    }

    /**
     * Create an instance of {@link Identification.Result }
     * 
     */
    public Identification.Result createIdentificationResult() {
        return new Identification.Result();
    }

    /**
     * Create an instance of {@link Identification.Identifiers }
     * 
     */
    public Identification.Identifiers createIdentificationIdentifiers() {
        return new Identification.Identifiers();
    }

    /**
     * Create an instance of {@link Identification.References }
     * 
     */
    public Identification.References createIdentificationReferences() {
        return new Identification.References();
    }

    /**
     * Create an instance of {@link Gathering.Permits }
     * 
     */
    public Gathering.Permits createGatheringPermits() {
        return new Gathering.Permits();
    }

    /**
     * Create an instance of {@link Gathering.OtherMeasurementsOrFacts }
     * 
     */
    public Gathering.OtherMeasurementsOrFacts createGatheringOtherMeasurementsOrFacts() {
        return new Gathering.OtherMeasurementsOrFacts();
    }

    /**
     * Create an instance of {@link Gathering.MultimediaObjects }
     * 
     */
    public Gathering.MultimediaObjects createGatheringMultimediaObjects() {
        return new Gathering.MultimediaObjects();
    }

    /**
     * Create an instance of {@link Gathering.Synecology.AssociatedTaxa }
     * 
     */
    public Gathering.Synecology.AssociatedTaxa createGatheringSynecologyAssociatedTaxa() {
        return new Gathering.Synecology.AssociatedTaxa();
    }

    /**
     * Create an instance of {@link Gathering.Biotope.MeasurementsOrFacts }
     * 
     */
    public Gathering.Biotope.MeasurementsOrFacts createGatheringBiotopeMeasurementsOrFacts() {
        return new Gathering.Biotope.MeasurementsOrFacts();
    }

    /**
     * Create an instance of {@link Gathering.CoordinateSets.CoordinateSet.References }
     * 
     */
    public Gathering.CoordinateSets.CoordinateSet.References createGatheringCoordinateSetsCoordinateSetReferences() {
        return new Gathering.CoordinateSets.CoordinateSet.References();
    }

    /**
     * Create an instance of {@link Gathering.CoordinateSets.CoordinateSet.CoordinatesUTM }
     * 
     */
    public Gathering.CoordinateSets.CoordinateSet.CoordinatesUTM createGatheringCoordinateSetsCoordinateSetCoordinatesUTM() {
        return new Gathering.CoordinateSets.CoordinateSet.CoordinatesUTM();
    }

    /**
     * Create an instance of {@link Gathering.CoordinateSets.CoordinateSet.CoordinatesGrid }
     * 
     */
    public Gathering.CoordinateSets.CoordinateSet.CoordinatesGrid createGatheringCoordinateSetsCoordinateSetCoordinatesGrid() {
        return new Gathering.CoordinateSets.CoordinateSet.CoordinatesGrid();
    }

    /**
     * Create an instance of {@link Gathering.CoordinateSets.CoordinateSet.CoordinatesLatLong }
     * 
     */
    public Gathering.CoordinateSets.CoordinateSet.CoordinatesLatLong createGatheringCoordinateSetsCoordinateSetCoordinatesLatLong() {
        return new Gathering.CoordinateSets.CoordinateSet.CoordinatesLatLong();
    }

    /**
     * Create an instance of {@link Gathering.NamedPlaceRelations.NamedPlaceRelation }
     * 
     */
    public Gathering.NamedPlaceRelations.NamedPlaceRelation createGatheringNamedPlaceRelationsNamedPlaceRelation() {
        return new Gathering.NamedPlaceRelations.NamedPlaceRelation();
    }

    /**
     * Create an instance of {@link Gathering.NamedAreas.NamedArea }
     * 
     */
    public Gathering.NamedAreas.NamedArea createGatheringNamedAreasNamedArea() {
        return new Gathering.NamedAreas.NamedArea();
    }

    /**
     * Create an instance of {@link Gathering.Project.ResourceURIs }
     * 
     */
    public Gathering.Project.ResourceURIs createGatheringProjectResourceURIs() {
        return new Gathering.Project.ResourceURIs();
    }

    /**
     * Create an instance of {@link Gathering.GatheringAgents.GatheringAgent }
     * 
     */
    public Gathering.GatheringAgents.GatheringAgent createGatheringGatheringAgentsGatheringAgent() {
        return new Gathering.GatheringAgents.GatheringAgent();
    }

    /**
     * Create an instance of {@link DataSets.DataSet.ResourceURIs }
     * 
     */
    public DataSets.DataSet.ResourceURIs createDataSetsDataSetResourceURIs() {
        return new DataSets.DataSet.ResourceURIs();
    }

    /**
     * Create an instance of {@link DataSets.DataSet.TechnicalContacts }
     * 
     */
    public DataSets.DataSet.TechnicalContacts createDataSetsDataSetTechnicalContacts() {
        return new DataSets.DataSet.TechnicalContacts();
    }

    /**
     * Create an instance of {@link DataSets.DataSet.ContentContacts }
     * 
     */
    public DataSets.DataSet.ContentContacts createDataSetsDataSetContentContacts() {
        return new DataSets.DataSet.ContentContacts();
    }

    /**
     * Create an instance of {@link DataSets.DataSet.OtherProviders }
     * 
     */
    public DataSets.DataSet.OtherProviders createDataSetsDataSetOtherProviders() {
        return new DataSets.DataSet.OtherProviders();
    }

    /**
     * Create an instance of {@link DataSets.DataSet.Units }
     * 
     */
    public DataSets.DataSet.Units createDataSetsDataSetUnits() {
        return new DataSets.DataSet.Units();
    }

}

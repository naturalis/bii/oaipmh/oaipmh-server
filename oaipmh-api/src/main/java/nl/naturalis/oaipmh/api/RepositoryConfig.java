package nl.naturalis.oaipmh.api;


import org.klojang.check.Check;

import static org.klojang.check.CommonChecks.gt;
import static org.klojang.check.CommonProperties.unbox;

/**
 * Java bean modeling the configuration for either a single OAI repository or a repository
 * group. In the latter case the {@link #getPageSize() pageSize} and
 * {@link #getTokenParser() tokenParser} properties must be understood as defaults for all
 * repositories in the group. Subclasses of {@code RepositoryConfig} could, if necessary,
 * add properties fine-tuning the behaviour of the individual repositories within the
 * repository group.
 *
 * @author Ayco Holleman
 */
public abstract class RepositoryConfig {

  private Integer pageSize;
  private String tokenParser;

  public RepositoryConfig() { }

  /**
   * Returns the desired maximum number of OAI-PMH records returned per request. Defaults
   * to 100.
   *
   * @return
   */
  public Integer getPageSize() {
    if (pageSize == null) {
      return 100;
    }
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    Check.notNull(pageSize, "pageSize").has(unbox(), gt(), 0);
    this.pageSize = pageSize;
  }

  /**
   * Returns the fully-qualified name of an alternative {@link ResumptionTokenParser}
   * implementation to be used for the OAI repository. If no resumption token parser is
   * specified in the configuration file, the
   * {@link ResumptionTokenParser#getDefaultParser() default parser} will be used.
   *
   * @return
   */
  public String getTokenParser() {
    return tokenParser;
  }

  public void setTokenParser(String parserClassName) {
    this.tokenParser = parserClassName;
  }

}

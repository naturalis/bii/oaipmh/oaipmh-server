package nl.naturalis.oaipmh.api;

import static java.util.stream.Collectors.joining;
import java.util.Collections;
import java.util.List;
import org.openarchives.oai._2.OAIPMHerrorType;

/**
 * An {@code OaiException} is thrown in response to an error condition
 * explicitly mentioned by the OAI-PMH specification. These error conditions do
 * <i>not</i> result in abnormal HTTP responses. The server still responds with
 * HTTP 200 (OK), and the response body still contains valid OAI-PMH XML (albeit
 * with &lt;error&gt; elements). Error conditions not explicitly mentioned in
 * the specifications result in a {@link RepositoryException}. An
 * {@code OaiException} can be thrown for more than one error, i.e. you can save
 * up all errors encountered while processing the request, and then throw one
 * {@code OaiException} containing all of these errors. This is conform the
 * specifications, which explicitly discourage fail-fast implementations.
 * 
 * @see http
 *      ://www.openarchives.org/OAI/openarchivesprotocol.html#ErrorConditions
 * 
 * @author Ayco Holleman
 *
 */
public class OaiProtocolException extends Exception {

	private final List<OAIPMHerrorType> errors;

	public OaiProtocolException(OAIPMHerrorType error) {
		super(getErrorAsString(error));
		errors = Collections.singletonList(error);
	}

	public OaiProtocolException(List<OAIPMHerrorType> errors) {
		super(errors.stream().map(OaiProtocolException::getErrorAsString)
				.collect(joining(": ")));
		this.errors = errors;
	}

	/**
	 * Returns the errors captured by this instance.
	 * 
	 * @return
	 */
	public List<OAIPMHerrorType> getErrors() {
		return errors;
	}

	/**
	 * Returns the first error captured by this instance. Convenient if you know it
	 * is the only error captured by this <code>OaiException</code>.
	 * 
	 * @return
	 */
	public OAIPMHerrorType getError() {
		return errors.get(0);
	}

	/**
	 * Returns a string representation of the errors.
	 * 
	 * @return
	 */
	public String getErrorsAsString() {
		return errors.stream().map(OaiProtocolException::getErrorAsString)
				.collect(joining(": "));
	}

	/**
	 * Returns a string representation of the first error.
	 * 
	 * @return
	 */
	public String getErrorAsString() {
		return getErrorAsString(getError());
	}

	private static String getErrorAsString(OAIPMHerrorType error) {
		StringBuilder sb = new StringBuilder();
		sb.append(error.getCode());
		if (error.getValue() != null) {
			sb.append(": ").append(error.getValue());
		}
		return sb.toString();
	}
}

package nl.naturalis.oaipmh.api;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Can be used as an abstract base class for OAI repositories. All methods are either
 * no-ops or throw an {@link UnsupportedOperationException}, except
 * {@link #newRequest(OaiRequest) newOaiRequest}, which stores the provided
 * {@link OaiRequest} instance into a protected field, and
 * {@link #getResumptionTokenParser() getResumptionTokenParser}, which returns the
 * {@link ResumptionToken default resumtion token parser}.
 *
 * @author Ayco Holleman
 */
public abstract class AbstractOaiRepository implements OaiRepository {

  protected OaiRequest oaiRequest;

  @Override
  public void newRequest(OaiRequest request) {
    this.oaiRequest = request;
  }

  @Override
  public InputStream getXsdForMetadataPrefix(String metadataPrefix)
      throws RepositoryException {
    throw new XsdNotFoundException(metadataPrefix);
  }

  @Override
  public ResumptionTokenParser getResumptionTokenParser() {
    return ResumptionTokenParser.getDefaultParser();
  }

  @Override
  public void getRecord(OutputStream output)
      throws OaiProtocolException, RepositoryException,
      UnsupportedOperationException {
    throw new UnsupportedOperationException("GetRecord not implemented");
  }

  @Override
  public void listRecords(OutputStream output)
      throws OaiProtocolException, RepositoryException,
      UnsupportedOperationException {
    throw new UnsupportedOperationException("ListRecords not implemented");
  }

  @Override
  public void listIdentifiers(OutputStream output)
      throws OaiProtocolException, RepositoryException,
      UnsupportedOperationException {
    throw new UnsupportedOperationException("GetRecord not implemented");
  }

  @Override
  public void listMetaDataFormats(OutputStream output)
      throws OaiProtocolException, RepositoryException,
      UnsupportedOperationException {
    throw new UnsupportedOperationException("ListMetaDataFormats not implemented");
  }

  @Override
  public void listSets(OutputStream output)
      throws OaiProtocolException, RepositoryException,
      UnsupportedOperationException {
    throw new UnsupportedOperationException("ListSets not implemented");
  }

  @Override
  public void identify(OutputStream output)
      throws OaiProtocolException, RepositoryException,
      UnsupportedOperationException {
    throw new UnsupportedOperationException("Identify not implemented");
  }

}

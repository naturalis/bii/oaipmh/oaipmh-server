package nl.naturalis.geneious.oaipmh;

/**
 * Models the contents of the plugin_document_xml column in case the root element is
 * &lt;DefaultAlignmentDocument&gt;.
 *
 * @author Ayco Holleman
 */
public final class DefaultAlignmentDocument extends PluginDocumentXml {

  private boolean contig;

  public DefaultAlignmentDocument() { }

  public boolean isContig() {
    return contig;
  }

  public void setContig(boolean contig) {
    this.contig = contig;
  }

}

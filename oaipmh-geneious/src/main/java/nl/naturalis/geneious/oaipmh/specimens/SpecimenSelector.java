package nl.naturalis.geneious.oaipmh.specimens;

import nl.naturalis.geneious.oaipmh.AnnotatedDocument;
import nl.naturalis.geneious.oaipmh.DocumentNotes;

import java.util.Comparator;

import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.RegistrationNumberCode_Samples;
import static nl.naturalis.geneious.oaipmh.util.GeneiousOaiUtil.discardOldest;
import static nl.naturalis.geneious.oaipmh.util.GeneiousOaiUtil.sameNote;

/**
 * A {@link Comparator} function for {@link AnnotatedDocument} instances that, per
 * specimen, selects the instance with the highest database ID.
 *
 * @author Ayco Holleman
 * @see SpecimenSetFilter
 */
final class SpecimenSelector {

  private SpecimenSelector() { }

  static void select(AnnotatedDocument doc0, AnnotatedDocument doc1) {
    DocumentNotes notes0 = doc0.getDocumentXml().getNotes();
    DocumentNotes notes1 = doc1.getDocumentXml().getNotes();
    if (sameNote(notes0, notes1, RegistrationNumberCode_Samples)) {
      discardOldest(doc0, doc1);
    }
  }

}

SELECT m.id, m.regno, m.producer, m.www_dir, m.www_file, m.www_published as date_modified, false as deleted
  FROM media m
 WHERE m.www_ok = 1
~%%begin:producers%
   AND m.producer IN(~%value%)
~%%end:producers%
~%%begin:from%
   AND m.www_published >= ~%value%
~%%end:from%
~%%begin:until%
   AND m.www_published <= ~%value%
~%%end:until%
~%%begin:regno%
   AND (m.regno = ~%exact% OR m.regno LIKE ~%like%)
~%%end:regno%
~%%begin:limit%
 LIMIT ~%from%, ~%to%
~%%end:limit%

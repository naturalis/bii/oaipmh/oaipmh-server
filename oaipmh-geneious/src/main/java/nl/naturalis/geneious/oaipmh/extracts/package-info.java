/**
 * 
 * Classes for the DNA extracts repository.
 * 
 * @author Ayco Holleman
 *
 */
package nl.naturalis.geneious.oaipmh.extracts;
CREATE TABLE public.folder (
    id int4 NOT NULL,
    g_group_id int4 NOT NULL,
    parent_folder_id int4 NULL,
    visible bool NOT NULL,
    modified timestamp NOT NULL,
    "name" varchar(255) NULL,
    CONSTRAINT folder_check CHECK ((id <> parent_folder_id)),
    CONSTRAINT folder_pkey PRIMARY KEY (id)
);

CREATE TABLE public.annotated_document (
	id int4 NOT NULL,
	folder_id int4 NOT NULL,
	modified timestamp NOT NULL,
	urn varchar(255) NOT NULL,
	document_xml text NOT NULL,
	plugin_document_xml text NOT NULL,
	reference_count int4 NOT NULL,
	CONSTRAINT annotated_document_pkey PRIMARY KEY (id),
	CONSTRAINT annotated_document_urn_key UNIQUE (urn)
);
CREATE INDEX ad_folder_id_fk ON public.annotated_document USING btree (folder_id);


CREATE TABLE public.boolean_search_field_value (
	id int4 NOT NULL,
	annotated_document_id int4 NOT NULL,
	search_field_code varchar(255) NOT NULL,
	value bool NOT NULL,
	CONSTRAINT boolean_search_field_value_pkey PRIMARY KEY (id)
);
CREATE INDEX bsfv_annotated_document_id_fk ON public.boolean_search_field_value USING btree (annotated_document_id);
CREATE INDEX bsfv_search_field_code_fk ON public.boolean_search_field_value USING btree (search_field_code);


CREATE TABLE public.date_search_field_value (
	id int4 NOT NULL,
	annotated_document_id int4 NOT NULL,
	search_field_code varchar(255) NOT NULL,
	value date NOT NULL,
	CONSTRAINT date_search_field_value_pkey PRIMARY KEY (id)
);
CREATE INDEX dtsfv_annotated_document_id_fk ON public.date_search_field_value USING btree (annotated_document_id);
CREATE INDEX dtsfv_search_field_code_fk ON public.date_search_field_value USING btree (search_field_code);


CREATE TABLE public.string_search_field_value (
	id int4 NOT NULL,
	annotated_document_id int4 NOT NULL,
	search_field_code varchar(255) NOT NULL,
	value text NOT NULL,
	CONSTRAINT string_search_field_value_pkey PRIMARY KEY (id)
);
CREATE INDEX ssfv_annotated_document_id_fk ON public.string_search_field_value USING btree (annotated_document_id);
CREATE INDEX ssfv_search_field_code_fk ON public.string_search_field_value USING btree (search_field_code);


COPY annotated_document FROM '/docker-entrypoint-initdb.d/annotated_document.csv' DELIMITER ',' CSV HEADER;
COPY folder FROM '/docker-entrypoint-initdb.d/folder.csv' DELIMITER ',' CSV HEADER;
COPY boolean_search_field_value FROM '/docker-entrypoint-initdb.d/boolean_search_field_value.csv' DELIMITER ',' CSV HEADER;
COPY date_search_field_value FROM '/docker-entrypoint-initdb.d/date_search_field_value.csv' DELIMITER ',' CSV HEADER;
COPY string_search_field_value FROM '/docker-entrypoint-initdb.d/string_search_field_value.csv' DELIMITER ',' CSV HEADER;

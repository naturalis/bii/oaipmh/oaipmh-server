package nl.naturalis.geneious.oaipmh.extracts;

import nl.naturalis.geneious.oaipmh.AnnotatedDocument;
import nl.naturalis.geneious.oaipmh.SetFilter;
import nl.naturalis.oaipmh.api.OaiRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.toList;

/**
 * A {@link SetFilter set-filter} for DNA extracts. Ensures that for each DNA extract
 * only one record from the annotated_document table is selected.
 *
 * @author Ayco Holleman
 */
public class ExtractSetFilter implements SetFilter {

  private static final Logger logger = LoggerFactory.getLogger(ExtractSetFilter.class);

  private static final int ASYNC_TRESHOLD = 1000;
  private static final int BATCH_SIZE = 2000;

  public ExtractSetFilter() {}

  @Override
  public List<AnnotatedDocument> filter(List<AnnotatedDocument> input) {

    logger.debug("Preparing filter");
    Collections.sort(input, Comparator.comparing(AnnotatedDocument::getExtractId));

    logger.debug("Running filter");
    if (input.size() < ASYNC_TRESHOLD) {
      process(input, 0, input.size());
    } else {
      runAsync(input);
    }

    logger.debug("Removing discarded documents");
    return input.stream()
        .filter(not(AnnotatedDocument::isDiscarded))
        .collect(toList());
  }

  private static void runAsync(List<AnnotatedDocument> input) {
    List<CompletableFuture<Void>> futures = new ArrayList<>();
    int i = 0;
    while (i < input.size()) {
      final int offset = i;
      int x = Math.min(i + BATCH_SIZE, input.size());
      String s = input.get(x - 1).getExtractId();
      for (int y = x; y < input.size(); ++y) {
        if (input.get(y).getExtractId().equals(s)) {
          ++x;
        } else {
          break;
        }
      }
      final int len = i = x;
      futures.add(CompletableFuture.runAsync(() -> process(input, offset, len),
          Executors.newCachedThreadPool()));
    }
    CompletableFuture<Void> all = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
    try {
      all.get();
    } catch (InterruptedException e) {
      logger.warn("{} interrupted", ExtractSetFilter.class.getName());
      Thread.currentThread().interrupt();
    } catch (ExecutionException e) {
      throw new OaiRuntimeException(e);
    }
  }

  private static void process(List<AnnotatedDocument> input, int offset, int len) {
    for (; offset < len; ++offset) {
      if (!input.get(offset).isDiscarded()) {
        boolean allDiscarded = true;
        for (int j = offset + 1; j < len; ++j) {
          if (!input.get(j).isDiscarded()) {
            ExtractSelector.select(input.get(offset), input.get(j));
            if (!input.get(j).isDiscarded()) {
              allDiscarded = false;
            } else if (allDiscarded && input.get(offset).isDiscarded()) {
              ++offset;
            }
          }
        }
        if (allDiscarded) {
          break;
        }
      }
    }
  }

}

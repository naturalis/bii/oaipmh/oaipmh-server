package nl.naturalis.oaipmh.server.resource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyWriter;

@Produces({MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN})
public class OaiMessageBodyWriter implements MessageBodyWriter<ByteArrayOutputStream> {

  public OaiMessageBodyWriter() {}

  @Override
  public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
    return type == ByteArrayOutputStream.class;
  }

  @Override
  public void writeTo(ByteArrayOutputStream baos, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
      MultivaluedMap<String, Object> httpHeaders, OutputStream out) throws IOException {
    baos.writeTo(out);
  }

}

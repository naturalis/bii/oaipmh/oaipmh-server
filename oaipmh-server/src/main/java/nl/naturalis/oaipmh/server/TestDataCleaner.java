package nl.naturalis.oaipmh.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Removes bloat from the export of the annotated_document table. The gitleaks scanner
 * (wrongly) thinks it contains API keys. If you make a new export to
 * annotated_document.csv (see /test-data/geneious/init/extract.txt), run this program,
 * throw away annotated_document.csv, and rename annotated_document-2.csv to
 * annotated_document.csv
 */
public class TestDataCleaner {
  static final String REPO_ROOT = "/home/ayco/git-repos/naturalis/oaipmh/oaipmh-pom";

  public static void main(String[] args) throws IOException {
    String path = REPO_ROOT + "/test-data/geneious/init/annotated_document.csv";
    String s = Files.readString(Path.of(path));
    String regex = "(?ms)<graphProperties(.+?)<\\/graphProperties>";
    s = s.replaceAll(regex, "");
    regex = "(?ms)<charSequence(.+?)<\\/charSequence>";
    s = s.replaceAll(regex, "");
    regex = "(?ms)<sequence_residues(.+?)<\\/sequence_residues>";
    s = s.replaceAll(regex, "");
    path = REPO_ROOT + "/test-data/geneious/init/annotated_document-2.csv";
    Files.writeString(Path.of(path), s);
  }
}

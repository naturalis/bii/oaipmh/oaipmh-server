package nl.naturalis.geneious.oaipmh.plates;

import nl.naturalis.geneious.oaipmh.GeneiousOaiRepository;
import nl.naturalis.geneious.oaipmh.GeneiousRepositoryConfig;
import nl.naturalis.geneious.oaipmh.ListRecordsHandler;

public class PlateRepository extends GeneiousOaiRepository {

  public PlateRepository(GeneiousRepositoryConfig config) {
    super(config);
  }

  @Override
  protected ListRecordsHandler newListRecordsHandler(GeneiousRepositoryConfig config) {
    return new PlateListRecordsHandler(config);
  }

}

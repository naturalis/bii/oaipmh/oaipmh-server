package nl.naturalis.oaipmh.server.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import nl.naturalis.oaipmh.api.ManagedRepository;
import nl.naturalis.oaipmh.server.OaiServer;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.klojang.util.ExceptionMethods;
import org.klojang.util.StringMethods;
import org.openarchives.oai._2.VerbType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;

public class OaiServerUtil {

  public static final String ERROR_ICON = ":red_circle:";
  public static final String WARN_ICON = ":large_orange_circle:";
  public static final String INFO_ICON = ":large_green_circle:";
  public static final String REQUEST_ICON = ":arrow_right:";

  private static final Logger logger = LoggerFactory.getLogger(OaiServerUtil.class);
  // The code that we are especially interested in when debugging
  private static final String EXCEPTION_ORIGIN = "nl.naturalis";

  public static Response showHomePage(ManagedRepository repo) {
    return Response.ok()
          .entity(new HomePageGenerator().getHomePage(repo))
          .type(MediaType.TEXT_HTML_TYPE)
          .build();
  }

  public static void broadcastRequest(String msg) {
    broadcast(REQUEST_ICON, msg);
  }

  public static void broadcastInfo(String msg) {
    broadcast(INFO_ICON, msg);
  }

  public static void broadcastWarning(String msg) {
    broadcast(WARN_ICON, msg);
  }

  public static void broadcastError(String msg) {
    broadcastError(msg, null);
  }

  public static void broadcastError(String msg, Throwable t) {
    if (t != null) {
      msg += ". " + ExceptionMethods.getDetailedMessage(t, EXCEPTION_ORIGIN);
    }
    broadcast(ERROR_ICON, msg);
  }

  public static void broadcast(String iconEscapeSequence, String msg) {
    try (CloseableHttpClient client = HttpClients.createDefault()) {
      String hook = OaiServer.OAI_CONFIG.messagingWebHook;
      HttpPost post = new HttpPost(hook);
      String thread = Thread.currentThread().getName();
      String msgPrefix = StringMethods.substringBefore(thread, " ", 1);
      msg = iconEscapeSequence + "  [" + msgPrefix + "] " + msg;
      ObjectMapper om = new ObjectMapper();
      String payload = om.writeValueAsString(Map.of("text", msg));
      StringEntity entity = new StringEntity(payload, APPLICATION_JSON);
      post.setEntity(entity);
      try (CloseableHttpResponse response = client.execute(post)) {
        if (response.getStatusLine().getStatusCode() >= 400) {
          logger.error(EntityUtils.toString(response.getEntity(), UTF_8));
        }
      }
    } catch (IOException e) {
      logger.error("broadcast failure", e);
    }
  }

  public static String createErrorMessage(Throwable throwable,
        String repoId,
        VerbType verb) {
    return String.format("Error while executing %s on repository %s: %s",
          verb.value(),
          repoId,
          ExceptionMethods.getDetailedMessage(throwable, EXCEPTION_ORIGIN));
  }

  public static String createErrorMessage(Throwable throwable, UriInfo uriInfo) {
    return String.format("Error while executing %s: %s",
          uriInfo.getBaseUri().relativize(uriInfo.getRequestUri()),
          ExceptionMethods.getDetailedMessage(throwable, EXCEPTION_ORIGIN));
  }

}

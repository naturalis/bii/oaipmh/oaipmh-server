//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.2 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.02.07 at 09:07:12 PM CET 
//


package org.tdwg.abcd2;

import java.math.BigInteger;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * A first proposal for this type of data. Note that this is used in the context of a collection unit and thus automatically refers to that unit. If your unit is a DNA Sample than please use the GGBN extension. This container here is meant to be used e.g. to provide Sequence information related to a specimen only without any further details.
 * 
 * <p>Java class for Sequence complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Sequence"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceDatabase" type="{http://www.tdwg.org/schemas/abcd/3.0}StringL"/&gt;
 *         &lt;element name="ID" type="{http://www.tdwg.org/schemas/abcd/3.0}String"/&gt;
 *         &lt;element name="Method" type="{http://www.tdwg.org/schemas/abcd/3.0}StringL" minOccurs="0"/&gt;
 *         &lt;element name="SequencedPart" type="{http://www.tdwg.org/schemas/abcd/3.0}StringL" minOccurs="0"/&gt;
 *         &lt;element name="Reference" type="{http://www.tdwg.org/schemas/abcd/3.0}Reference" minOccurs="0"/&gt;
 *         &lt;element name="SequencingAgent" type="{http://www.tdwg.org/schemas/abcd/3.0}Contact" minOccurs="0"/&gt;
 *         &lt;element name="SequenceLength" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="DirectAccessURL" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sequence", propOrder = {
    "sequenceDatabase",
    "id",
    "method",
    "sequencedPart",
    "reference",
    "sequencingAgent",
    "sequenceLength",
    "directAccessURL"
})
public class Sequence {

    @XmlElement(name = "SequenceDatabase", required = true)
    protected StringL sequenceDatabase;
    @XmlElement(name = "ID", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String id;
    @XmlElement(name = "Method")
    protected StringL method;
    @XmlElement(name = "SequencedPart")
    protected StringL sequencedPart;
    @XmlElement(name = "Reference")
    protected Reference reference;
    @XmlElement(name = "SequencingAgent")
    protected Contact sequencingAgent;
    @XmlElement(name = "SequenceLength")
    protected BigInteger sequenceLength;
    @XmlElement(name = "DirectAccessURL")
    @XmlSchemaType(name = "anyURI")
    protected String directAccessURL;

    /**
     * Gets the value of the sequenceDatabase property.
     * 
     * @return
     *     possible object is
     *     {@link StringL }
     *     
     */
    public StringL getSequenceDatabase() {
        return sequenceDatabase;
    }

    /**
     * Sets the value of the sequenceDatabase property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringL }
     *     
     */
    public void setSequenceDatabase(StringL value) {
        this.sequenceDatabase = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the method property.
     * 
     * @return
     *     possible object is
     *     {@link StringL }
     *     
     */
    public StringL getMethod() {
        return method;
    }

    /**
     * Sets the value of the method property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringL }
     *     
     */
    public void setMethod(StringL value) {
        this.method = value;
    }

    /**
     * Gets the value of the sequencedPart property.
     * 
     * @return
     *     possible object is
     *     {@link StringL }
     *     
     */
    public StringL getSequencedPart() {
        return sequencedPart;
    }

    /**
     * Sets the value of the sequencedPart property.
     * 
     * @param value
     *     allowed object is
     *     {@link StringL }
     *     
     */
    public void setSequencedPart(StringL value) {
        this.sequencedPart = value;
    }

    /**
     * Gets the value of the reference property.
     * 
     * @return
     *     possible object is
     *     {@link Reference }
     *     
     */
    public Reference getReference() {
        return reference;
    }

    /**
     * Sets the value of the reference property.
     * 
     * @param value
     *     allowed object is
     *     {@link Reference }
     *     
     */
    public void setReference(Reference value) {
        this.reference = value;
    }

    /**
     * Gets the value of the sequencingAgent property.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getSequencingAgent() {
        return sequencingAgent;
    }

    /**
     * Sets the value of the sequencingAgent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setSequencingAgent(Contact value) {
        this.sequencingAgent = value;
    }

    /**
     * Gets the value of the sequenceLength property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceLength() {
        return sequenceLength;
    }

    /**
     * Sets the value of the sequenceLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceLength(BigInteger value) {
        this.sequenceLength = value;
    }

    /**
     * Gets the value of the directAccessURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectAccessURL() {
        return directAccessURL;
    }

    /**
     * Sets the value of the directAccessURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectAccessURL(String value) {
        this.directAccessURL = value;
    }

}
